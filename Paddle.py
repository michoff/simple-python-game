import pygame


class Paddle(pygame.sprite.Sprite):
    """
    Simple Klasse, welche einen Balken erzeugt, der der Maus horizontal folgt
    """
    def __init__(self, filename, screen):
        self.screen = screen
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.image = pygame.image.load(filename).convert()
        self.rect = self.image.get_rect()
        self.rect.bottom = self.screen.bottom - 20  # Distanz zum unteren Bildschimrand

    def update(self):
        self.rect.centerx = pygame.mouse.get_pos()[0]  # der Block folgt der Maus
        self.rect.clamp_ip(self.screen)  # "verhindert", dass der Rand des Balkens das Spielfenster verlÃ¤sst
