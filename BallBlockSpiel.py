#!/usr/bin/python
# -*- encode: utf-8 -*-
__author__ = 'michael hoffmann'
#########################################

import pygame
from pygame.locals import *
import sys
import pygame.mixer
from Paddle import Paddle
from Ball import Ball
from Block import Block
from Score import Score


# "globale" Konstante als Grundlage fÃ¼r weitere Klassen und Fenster
SCREEN = Rect(0, 0, 380, 400)


# def main, da auf diese Weise das Programm auch extern genutzt werden kann
def main():
    # mixer.init() vor pygame.init(), da mixer sonst keine Buffer zugeordnet werden
    pygame.mixer.init(frequency=22050, size=-16, channels=2, buffer=512)
    pygame.init()

    screen = pygame.display.set_mode(SCREEN.size)

    # Schmutzige Sprite Gruppierung erstellen
    group = pygame.sprite.RenderUpdates()

    # Saubere Sprite Gruppe fÃ¼r die BlÃ¶cke
    blocks = pygame.sprite.Group()

    # die Container der relevanten Klassen erhalten die schmutzige Gruppierung
    Paddle.containers = group
    Ball.containers = group
    Block.containers = group, blocks

    # Erstellt den Spieler Block
    paddle = Paddle("img/paddle.png", SCREEN)

    # fÃ¼llt das Fenster mit BlÃ¶cken
    for x in range(1, 15):
        for y in range(1, 11):
            Block("img/block.png", x, y, SCREEN)

    # erstellt einen Score mit den Kooardianten (0, 0), somit am oberen linken Bildschirmrand
    score = Score(0, 0)

    # erstellt einen Spielball
    Ball("img/ball.png", paddle, blocks, score, 5, 135, 45, SCREEN)

    clock = pygame.time.Clock()

    while True:
        clock.tick(60)  # setzt die Bilderanzahl auf 60 / s
        screen.fill((0, 20, 0))
        # Aktualisierung der schmutzigen Gruppe
        group.update()
        # Die schmutzige Gruppe wird auf den Bildschirm wiedergegeben
        group.draw(screen)
        # Der aktuelle Score wird prÃ¤sentiert
        score.draw(screen)
        # das Spielfenster wird mit den vorherigen Ãnderungen aktualisiert
        pygame.display.update()

        # PrÃ¼ft ob der Spieler das Spiel beenden mÃ¶chte durch drÃ¼cken des ESC Buttons oder SchlieÃen des Fensters
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            if event.type == KEYDOWN and event.key == K_ESCAPE:
                pygame.quit()
                sys.exit()


if __name__ == "__main__":
    main()
