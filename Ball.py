#!/usr/bin/python
# -*- encode: utf-8 -*-
import pygame
import math


class Ball(pygame.sprite.Sprite):
    """
    Erstellt einen Ball, der auf BlÃ¶cke und das Spielfenster mit RichtungsÃ¤nderungen und Sound reagiert
    """
    def __init__(self, filename, paddle, blocks, score, speed, angle_left, angle_right, screen):
        pygame.sprite.Sprite.__init__(self, self.containers)  # die container werden von auÃen zugefÃ¼hrt
        self.screen = screen
        self.image = pygame.image.load(filename).convert()
        self.rect = self.image.get_rect()
        self.dx = self.dy = 0  # verschiebung von x und y initliasieren
        self.paddle = paddle  # Spieler Block Zuweisung fÃ¼r Kollisionen
        self.blocks = blocks  # Punkt BlÃ¶cke Zuweisung fÃ¼r Kollisionen
        self.update = self.start  # Ãberschreiben der update() Methode
        self.score = score  # erhalten des Scores, damit Punkte zugewisen werden kÃ¶nnen
        self.hit = 0  # hit-counter
        self.speed = speed  # Geschwindigkeit des Balls
        self.angle_left = angle_left  # Winkel links, Standard: 135Â°
        self.angle_right = angle_right  # Winkel rechts, Standard: 45Â°
        self.paddle_sound = pygame.mixer.Sound("wav/player_bar.wav")
        self.block_sound = pygame.mixer.Sound("wav/ball.wav")
        self.gameover_sound = pygame.mixer.Sound("wav/game_over.wav")

    # Ãberschriebene update() Methode, wegen schmutzigem redraw. Wird nur am Start jeder Runde ausgefÃ¼hrt
    def start(self):
        #
        self.rect.centerx = self.paddle.rect.centerx
        self.rect.bottom = self.paddle.rect.top

        # startet den Ball
        if pygame.mouse.get_pressed()[0] == 1:
            self.dx = 0
            self.dy = -self.speed
            self.update = self.move

    # bewegt den Ball
    def move(self):
        # setzt neue Positon
        self.rect.centerx += self.dx
        self.rect.centery += self.dy

        # PrÃ¼ft ob der Ball mit den SpielflÃ¤chenrÃ¤ndern kollidiert
        if self.rect.left < self.screen.left:  # linker Rand
            self.rect.left = self.screen.left
            self.dx = -self.dx  # Umkehrung der x Laufrichtung (Abprallen)
        if self.rect.right > self.screen.right:  # rechter Rand
            self.rect.right = self.screen.right
            self.dx = -self.dx  # Umkehrung der x Laufrichtung
        if self.rect.top < self.screen.top:  # oberer Rand
            self.rect.top = self.screen.top
            self.dy = -self.dy # Umkehrung der y Laufrichtung (Abprallen)

        # berechnet bei kollision den neuen Winkel
        if self.rect.colliderect(self.paddle.rect) and self.dy > 0:
            self.paddle_sound.play(loops=0, maxtime=0, fade_ms=0)  # Vorsorge gegen Sound Latenz
            self.hit = 0
            (x1, y1) = (self.paddle.rect.left - self.rect.width, self.angle_left)
            (x2, y2) = (self.paddle.rect.right, self.angle_right)
            x = self.rect.left  # x Koordinate der linken Ball Seite
            y = (float(y2 - y1) / (x2 - x1)) * (x - x1) + y1
            angle = math.radians(y)
            self.dx = self.speed * math.cos(angle)
            self.dy = -self.speed * math.sin(angle)

        # prÃ¼ft ob der Ball die SpielflÃ¤che nach unten Ã¼berschritten hat
        # Dies ist der Fall, wenn  das Paddel den Ball nicht getroffen hat
        if self.rect.top > self.screen.bottom:
            self.update = self.start  # beim aufruf von update, wird der Ball in den Startzustand versetzt
            self.gameover_sound.play(loops=0, maxtime=0, fade_ms=0)
            self.hit = 0
            self.score.add_score(-100)  # zieht 100 Punkte ab!

        # prÃ¼ft ob der Ball mit anderen Sprites kollidiert
        blocks_collided = pygame.sprite.spritecollide(self, self.blocks, True)
        if blocks_collided:  # wenn der Ball auf einen Sprite trifft
            oldrect = self.rect
            for block in blocks_collided:
                # der ball trifft den Block nur mit einem Teil seiner rechten HÃ¤lfte
                if oldrect.left < block.rect.left and oldrect.right < block.rect.right:
                    self.rect.right = block.rect.left
                    self.dx = -self.dx

                # der ball trifft den Block nur mit einem Teil seiner linken HÃ¤lfte
                if block.rect.left < oldrect.left and block.rect.right < oldrect.right:
                    self.rect.left = block.rect.right
                    self.dx = -self.dx

                # der ball trifft den Block nur mit einem teil seiner unteren HÃ¤lfte
                if oldrect.top < block.rect.top and oldrect.bottom < block.rect.bottom:
                    self.rect.bottom = block.rect.top
                    self.dy = -self.dy

                # der ball trifft den Block nur mit einem teil seiner oberen HÃ¤lfte
                if block.rect.top < oldrect.top and block.rect.bottom < oldrect.bottom:
                    self.rect.top = block.rect.bottom
                    self.dy = -self.dy

                self.block_sound.play(loops=0, maxtime=0, fade_ms=0)  # Sound Effekt beim Aufprall
                self.hit += 1  # Counter wird erhÃ¶ht
                self.score.add_score(self.hit * 10)  # Score wird in AbhÃ¤ngigkeit zum Counter erhÃ¶ht
