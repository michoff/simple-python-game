#!/usr/bin/python
# -*- encode: utf-8 -*-
import pygame


class Score:
    """
    Simple Klasse, welche den aktuellen Score auf dem Spielfenster anzeigen soll
    """
    def __init__(self, x, y):
        self.sysfont = pygame.font.SysFont("Arial", 15)
        self.score = 0
        (self.x, self.y) = (x, y)

    def draw(self, screen):
        img = self.sysfont.render("SCORE:" + str(self.score), True, (255, 255, 250))
        screen.blit(img, (self.x, self.y))

    def add_score(self, x):
        self.score += x
