#!/usr/bin/python
# -*- encode: utf-8 -*-

import pygame


class Block(pygame.sprite.Sprite):
    """
    erstellt einen Block, welcher als Punkt und als Ziel der ZerstÃ¶rung dient
    """
    def __init__(self, filename, x, y, screen):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.image = pygame.image.load(filename).convert()
        self.rect = self.image.get_rect()
        # Positionierung
        self.rect.left = screen.left + x * self.rect.width -20  # -20, damit die BlÃ¶cke am Bildschirmrand beginnen
        self.rect.top = screen.top + y * self.rect.height  # ohne Korrektur, da so noch Platz fÃ¼r den Score bleibt
